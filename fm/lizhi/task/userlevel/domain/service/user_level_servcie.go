package service

import (
	"encoding/json"
	"goskeleton/fm/lizhi/task/userlevel/domain/repository"
	"strconv"
)
import CommonInfrastructure "goskeleton/fm/lizhi/task/common/infrastructure"

type UserLevelService struct {
}

var appId = 6518293

func (userLevelService UserLevelService) GetUserLevelAwardInfo(userId int64) repository.UserLevelAwardInfoDTO {
	userLevelAwardInfoDTO := new(repository.UserLevelAwardInfoDTO)
	userLevelAwardInfoDTO.UserLevelInfo = *(userLevelService.queryUserLevelInfo(userId))
	userLevelAwardInfoDTO.Badges = userLevelService.queryUserLevelBrand()
	userLevelAwardInfoDTO.LevelTaskList = userLevelService.queryUserLevelRewardInfo(userId)
	return *userLevelAwardInfoDTO
}

/*
*
获取等级奖励
*/
func (userLevelService UserLevelService) ReceiveLevelTaskAwardService(userId int64, level int) bool {
	jsonStr := "{\"domain\":644,\"op\":503,\"serviceName\":\"fm.lizhi.live.amusement.task.api.UserLevelService\",\"methodName\":\"receiveLevelTaskAward\",\"request\":{\"req\":{\"level\":" + strconv.Itoa(level) + ",\"appId\":6518293,\"userId\":" + strconv.FormatInt(userId, 10) + "}}}"
	generalisationDetailsDTO := new(CommonInfrastructure.GeneralisationDetailsDTO)
	json.Unmarshal([]byte(jsonStr), &generalisationDetailsDTO)
	generalisationRes := CommonInfrastructure.GlobalHtpUtils.Generalisation(generalisationDetailsDTO)
	return generalisationRes.Code == 0
}

/*
*
获取用户当前的等级信息
*/
func (userLevelService UserLevelService) queryUserLevelInfo(userId int64) *repository.UserLevelDTO {

	jsonStr := "{\"domain\":644,\"op\":501,\"serviceName\":\"fm.lizhi.live.amusement.task.api.UserLevelService\",\"methodName\":\"queryUserLevelInfo\",\"request\":{\"req\":{\"appId\":6518293,\"userId\":" + strconv.FormatInt(userId, 10) + "}}}"
	generalisationDetailsDTO := new(CommonInfrastructure.GeneralisationDetailsDTO)
	json.Unmarshal([]byte(jsonStr), &generalisationDetailsDTO)
	generalisationRes := CommonInfrastructure.GlobalHtpUtils.Generalisation(generalisationDetailsDTO)
	if generalisationRes.Code == 0 {
		userLevelDTO := new(repository.UserLevelDTO)
		target := generalisationRes.Target
		resp := target["resp"].(map[string]interface{})
		userLevelDTO.CurrentLevel = int(resp["currentLevel"].(float64))
		userLevelDTO.MaxLevel = resp["maxLevel"].(bool)
		userLevelDTO.CurrentExperience = int64(resp["currentExperience"].(float64))
		userLevelDTO.TargetExperience = int64(resp["targetExperience"].(float64))
		return userLevelDTO
	} else {
		return nil
	}
}

/*
*
获取等级牌
*/
func (userLevelService UserLevelService) queryUserLevelRewardInfo(userId int64) []repository.LevelTaskDTO {

	jsonStr := "{\"domain\":644,\"op\":502,\"serviceName\":\"fm.lizhi.live.amusement.task.api.UserLevelService\",\"methodName\":\"queryUserLevelRewardInfo\",\"request\":{\"req\":{\"appId\":6518293,\"userId\":" + strconv.FormatInt(userId, 10) + "}}}"
	generalisationDetailsDTO := new(CommonInfrastructure.GeneralisationDetailsDTO)
	json.Unmarshal([]byte(jsonStr), &generalisationDetailsDTO)
	generalisationRes := CommonInfrastructure.GlobalHtpUtils.Generalisation(generalisationDetailsDTO)
	if generalisationRes.Code == 0 {
		target := generalisationRes.Target
		resps := target["resp"].([]interface{})
		levelTaskDTOs := make([]repository.LevelTaskDTO, len(resps))
		for index, tempResp := range resps {
			resp := tempResp.(map[string]interface{})
			levelTaskDTOs[index].Level = int(resp["level"].(float64))
			levelTaskDTOs[index].TaskStatus = int(resp["taskStatus"].(float64))
			awardMaps := resp["awards"].([]interface{})
			awardDTOs := make([]repository.AwardDTO, len(awardMaps))
			for index, tempAwardValue := range awardMaps {
				awardValue := tempAwardValue.(map[string]interface{})
				awardDTOs[index].AwardNum = int(awardValue["awardNum"].(float64))
				awardDTOs[index].AwardType = int(awardValue["awardType"].(float64))
				awardDTOs[index].AwardName = awardValue["awardName"].(string)
				awardDTOs[index].AwardIcon = awardValue["awardIcon"].(string)
			}
			levelTaskDTOs[index].Awards = awardDTOs
		}
		return levelTaskDTOs
	} else {
		return nil
	}
}

/*
*
查询 等级牌
*/
func (userLevelService UserLevelService) queryUserLevelBrand() []repository.UserLevelBrandDTO {

	jsonStr := "{\"domain\":644,\"op\":504,\"serviceName\":\"fm.lizhi.live.amusement.task.api.UserLevelService\",\"methodName\":\"queryUserLevelBrand\",\"request\":{}}"
	generalisationDetailsDTO := new(CommonInfrastructure.GeneralisationDetailsDTO)
	json.Unmarshal([]byte(jsonStr), &generalisationDetailsDTO)
	generalisationRes := CommonInfrastructure.GlobalHtpUtils.Generalisation(generalisationDetailsDTO)
	if generalisationRes.Code == 0 {
		target := generalisationRes.Target
		resps := target["resp"].([]interface{})
		userLevelBrandDTOs := make([]repository.UserLevelBrandDTO, len(resps))
		for index, resp := range resps {
			tempResp := resp.(map[string]interface{})
			userLevelBrandDTOs[index].BadgeIcon = tempResp["badgeIcon"].(string)
			userLevelBrandDTOs[index].Threshold = int(tempResp["threshold"].(float64))

		}
		return userLevelBrandDTOs
	} else {
		return nil
	}
}

var GlobalUserLevelService = new(UserLevelService)
