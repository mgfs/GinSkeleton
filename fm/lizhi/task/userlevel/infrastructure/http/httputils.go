package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type HttpUtils struct {
}

type GeneralisationDetailsDTO struct {
	Domain      int                    `json:"domain"`
	Op          int                    `json:"op"`
	ServiceName string                 `json:"serviceName"`
	MethodName  string                 `json:"methodName"`
	Request     map[string]interface{} `json:"request"`
}

type CommonRpcReturnDTO struct {
	Code       int                    `json:"code"`
	Attachment string                 `json:"attachment"`
	Target     map[string]interface{} `json:"target"`
}

//type CommonAny interface {
//}

type CommonReturnDTO[T any] struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    T      `json:"data"`
}

type GeneralisationReturnDTO struct {
	Code    int                `json:"domain"`
	Message string             `json:"serviceName"`
	Data    CommonRpcReturnDTO `json:"data"`
}

var sessionKeyMap = make(map[string]int64)

func main() {
	httpUtils := new(HttpUtils)
	//jsonStr := "{\"domain\":644,\"op\":501,\"serviceName\":\"fm.lizhi.live.amusement.task.api.UserLevelService\",\"methodName\":\"queryUserLevelInfo\",\"request\":{\"req\":{\"appId\":111,\"userId\":5280331036799009324}}}"
	//
	//g := new(GeneralisationDetailsDTO)
	//json.Unmarshal([]byte(jsonStr), &g)

	jsonStr := "{\"domain\":644,\"op\":504,\"serviceName\":\"fm.lizhi.live.amusement.task.api.UserLevelService\",\"methodName\":\"queryUserLevelRewardInfo\",\"request\":{\"req\":{\"appId\":111,\"userId\":5280331036799009324}}}"

	g := new(GeneralisationDetailsDTO)
	json.Unmarshal([]byte(jsonStr), &g)
	generalisation := httpUtils.Generalisation(g)
	fmt.Println(generalisation)

	//id := httpUtils.GetUserId("Za7uRqKxK6ht-crC5jzxR1WC77v8Wqmo")
	//fmt.Println(id)
	//id = httpUtils.GetUserId("Za7uRqKxK6ht-crC5jzxR1WC77v8Wqmo")
	//fmt.Println(id)
}

func (httpUtils HttpUtils) GetUserId(sessionKey string) int64 {
	mapRes := sessionKeyMap[sessionKey]
	if 0 != mapRes {
		return mapRes
	}
	url := "http://ppliveamuseadmin.yfxn.lizhi.fm/"
	url += "feishu/tools/getUserId?sessionKey=" + sessionKey
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	fmt.Println(resp.StatusCode)
	if resp.StatusCode == http.StatusOK {
		fmt.Println("ok")

		commonReturnDTO := CommonReturnDTO[int64]{}
		json.Unmarshal(body, &commonReturnDTO)
		if commonReturnDTO.Code == 0 {
			sessionKeyMap[sessionKey] = commonReturnDTO.Data
			return commonReturnDTO.Data
		}
	}

	return 0
}

func (httpUtils HttpUtils) Generalisation(generalisationDetailsDTO *GeneralisationDetailsDTO) CommonRpcReturnDTO {

	url := "http://ppliveamuseadmin.yfxn.lizhi.fm/"
	url += "feishu/tools/generalisation"
	body, _ := json.Marshal(generalisationDetailsDTO)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(body))

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		jsonStr := string(body)
		fmt.Println("Response: ", jsonStr)
		generalisationReturnDTO := new(GeneralisationReturnDTO)

		json.Unmarshal(body, &generalisationReturnDTO)
		return generalisationReturnDTO.Data

	} else {
		fmt.Println("Get failed with error: ", resp.Status)
	}

	res := new(CommonRpcReturnDTO)
	res.Code = 400
	return *res
}

var GlobalHtpUtils = new(HttpUtils)
