package facade

import (
	"github.com/gin-gonic/gin"
	"goskeleton/app/global/consts"
	"goskeleton/app/utils/response"
	CommonInfrastructure "goskeleton/fm/lizhi/task/common/infrastructure"
	"goskeleton/fm/lizhi/task/userlevel/domain/service"
)

type UserLevel struct {
}

// 1.获取用户等级数据
func (u *UserLevel) GetUserLevelAwardInfo(context *gin.Context) {

	//  由于本项目骨架已经将表单验证器的字段(成员)绑定在上下文，因此可以按照 GetString()、GetInt64()、GetFloat64（）等快捷获取需要的数据类型
	// 当然也可以通过gin框架的上下文原原始方法获取，例如： context.PostForm("name") 获取，这样获取的数据格式为文本，需要自己继续转换

	// 这里随便模拟一条数据返回
	token := context.GetHeader("token")
	userId := CommonInfrastructure.GlobalHtpUtils.GetUserId(token)
	if userId == 0 {
		response.Fail(context, 500, "没访问权限", gin.H{})
	} else {
		info := service.GlobalUserLevelService.GetUserLevelAwardInfo(userId)
		response.Success(context, "ok", info)
	}

}

// 1.领取奖励
func (u *UserLevel) ReceiveLevelTaskAward(context *gin.Context) {

	//  由于本项目骨架已经将表单验证器的字段(成员)绑定在上下文，因此可以按照 GetString()、GetInt64()、GetFloat64（）等快捷获取需要的数据类型
	// 当然也可以通过gin框架的上下文原原始方法获取，例如： context.PostForm("name") 获取，这样获取的数据格式为文本，需要自己继续转换

	// 这里随便模拟一条数据返回
	token := context.GetHeader("token")

	level := context.GetFloat64(consts.ValidatorPrefix + "level")

	userId := CommonInfrastructure.GlobalHtpUtils.GetUserId(token)
	if userId == 0 {
		response.Fail(context, 500, "没访问权限", gin.H{})
	} else {
		info := service.GlobalUserLevelService.ReceiveLevelTaskAwardService(userId, int(level))
		if info {
			response.Success(context, "ok", info)
		} else {
			response.Fail(context, 400, "领取失败", gin.H{})
		}

	}

}
