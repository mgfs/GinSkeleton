package repository

type AwardDTO struct {
	AwardName string `json:"awardName"`
	AwardIcon string `json:"awardIcon"`
	AwardNum  int    `json:"awardNum"`
	AwardType int    `json:"awardType"`
}

type LevelTaskDTO struct {
	Level      int        `json:"level"`
	Awards     []AwardDTO `json:"awards"`
	TaskStatus int        `json:"taskStatus"`
}
type UserLevelBrandDTO struct {
	Threshold int    `json:"threshold"`
	BadgeIcon string `json:"badgeIcon"`
}

type UserLevelDTO struct {
	CurrentLevel      int   `json:"currentLevel"`
	CurrentExperience int64 `json:"currentExperience"`
	TargetExperience  int64 `json:"targetExperience"`
	MaxLevel          bool  `json:"maxLevel"`
}

type UserLevelAwardInfoDTO struct {
	LevelTaskList []LevelTaskDTO      `json:"levelTaskList"`
	Badges        []UserLevelBrandDTO `json:"badges"`
	UserLevelInfo UserLevelDTO        `json:"userLevelInfo"`
}
